package main

import (
	"sync"

	"github.com/RumbleFrog/discordgo"
	"github.com/davecgh/go-spew/spew"
	"github.com/gotk3/gotk3/gtk"
)

var (
	currentChannel int64
)

func loadMsgs(chID int64) {
	currentChannel = 0

	children := chbox.GetChildren()
	children.Foreach(func(item interface{}) {
		chbox.Remove(item.(gtk.IWidget))
	})

	messages = []*MessageElements{}

	if ch, e := d.State.Channel(chID); e != nil {
		spew.Dump(ch.PermissionOverwrites)
	}

	msgs, err := d.ChannelMessages(chID, 25, 0, 0, 0)
	if err != nil {
		warn("Failed to fetch message", err.Error())
		return
	}

	// reverse
	for i := len(msgs)/2 - 1; i >= 0; i-- {
		opp := len(msgs) - 1 - i
		msgs[i], msgs[opp] = msgs[opp], msgs[i]
	}

	var (
		Mutex = &sync.Mutex{}
		subWG = sync.WaitGroup{}
	)

	for _, m := range msgs {
		if rstore.Check(m.Author, RelationshipBlocked) {
			continue
		}

		subWG.Add(1)
		go func(m *discordgo.Message) {
			defer subWG.Done()
			addMsg(m, false, Mutex)
		}(m)
	}

	currentChannel = chID

	scrollchat()

	subWG.Wait()
}
