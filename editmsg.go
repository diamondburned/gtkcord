package main

import (
	"log"
	"strconv"
	"unsafe"

	"github.com/RumbleFrog/discordgo"
	"github.com/davecgh/go-spew/spew"
	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
)

func EditMsg(s *discordgo.Session, m *discordgo.MessageUpdate) {
	if currentChannel != m.ChannelID {
		return
	}

	if rstore.Check(m.Author, RelationshipBlocked) {
		return
	}

	for _, msgElems := range messages {
		for _, elem := range msgElems.Messages {
			if elem.MessageID == m.ID {
				log.Println("Found edited message:", m.Content)
				messagebox, err := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 0)
				die(err)

				messagebox.SetHAlign(gtk.ALIGN_START)
				messagebox.SetVAlign(gtk.ALIGN_START)

				buildMsg(m.Message, messagebox, false, true, nil)

				boxtype := msgElems.MessagesBox.TypeFromInstance()

				// i := 0
				glib.IdleAdd(func() {
					msgElems.MessagesBox.GetChildren().Foreach(func(item interface{}) {
						if item.(*gtk.Widget).IsA(boxtype) {
							box := (*gtk.Box)(unsafe.Pointer(item.(*gtk.Widget)))

							if name, err := box.GetName(); err == nil {
								if i, err := strconv.ParseInt(name, 10, 64); err == nil {
									if i == m.ID {
										box.GetChildren().Foreach(func(item interface{}) {
											if item.(*gtk.Widget).IsA(boxtype) {
												child := (*gtk.Box)(unsafe.Pointer(item.(*gtk.Widget)))

												box.Remove(child)
												box.Add(messagebox)

												box.ShowAll()

												return
											}
										})
									}
								}
							}
						} else {
							log.Println("Not a box!")
							spew.Dump(item)
						}
					})
				})

				return
			}
		}
	}
}
