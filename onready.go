package main

import (
	"fmt"
	"log"
	"sort"
	"strings"
	"sync"

	"github.com/RumbleFrog/discordgo"
	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
)

// func onReady(s *discordgo.Session, r *discordgo.Ready) {
// 	log.Println("Discord is ready")

// 	for _, g := range d.State.Guilds {
// 		it := addRow(">", g.Name)

// 		for _, c := range g.Channels {
// 			_ = addSubRow(it, "#", c.Name)
// 		}
// 	}
// }

type sortCats struct {
	Channels      []*discordgo.Channel
	CategoryOrder int
	CategoryName  string
	CategoryID    int64
}

var (
	_Init = false

	DataSaving = false

	sema = make(chan struct{}, 8)
)

func onReady(s *discordgo.Session, r *discordgo.Ready) {
	log.Println("Discord is ready")

	rstore.Relationships = r.Relationships
	var guildIDs []int64

	if settings, err := s.UserSettings(); err == nil {
		switch settings.Status {
		case discordgo.StatusOnline:
			Busy = false
		default:
			Busy = true
		}

		setStatusButton()

		guildIDs = settings.GuildPositions
	} else {
		for _, g := range d.State.Guilds {
			guildIDs = append(guildIDs, g.ID)
		}
	}

	if !_Init {
		glib.IdleAdd(func(w *gtk.Window) {
			w.Remove(spinner)

			_, err := initWindow(w)
			if err != nil {
				log.Panicln("CAN'T INITIATE MAIN WINDOW! ALL HELL BREAKS LOOSE!", err)
			}

			w.ShowAll()

			it := addRow(nil, "Direct Messages", 0)

			for _, dmCh := range d.State.PrivateChannels {
				var userstr []string
				for _, u := range dmCh.Recipients {
					userstr = append(userstr, u.Username)
				}

				_ = addSubRow(it, nil, strings.Join(userstr, ", "), dmCh.ID)
			}

			for _, guildID := range guildIDs {
				server, err := d.State.Guild(guildID)
				if err != nil {
					warn("Failed to get guild", err.Error())
					continue
				}

				//                             v negative to differentiate
				//                             v between server and channel ID
				it := addRow(nil, server.Name, -server.ID)

				// map[ch_id][]channels
				categorizedChannels := make(map[int64]sortCats)
				categorizedChannels[0] = sortCats{
					CategoryName:  "",
					CategoryOrder: -1,
					Channels:      []*discordgo.Channel{},
				}

				for _, channel := range server.Channels {
					switch channel.Type {
					case discordgo.ChannelTypeGuildVoice:
						continue
					case discordgo.ChannelTypeGuildCategory:
						t := categorizedChannels[channel.ID]
						t.CategoryName = channel.Name
						t.CategoryOrder = channel.Position
						t.CategoryID = channel.ID
						categorizedChannels[channel.ID] = t
					}
				}

				for _, channel := range server.Channels {
					switch channel.Type {
					case discordgo.ChannelTypeGuildText:
						t := categorizedChannels[channel.ParentID]
						t.Channels = append(
							t.Channels,
							channel,
						)
						categorizedChannels[channel.ParentID] = t
					}
				}

				var sortedCats []sortCats
				for _, category := range categorizedChannels {
					sortedCats = append(sortedCats, category)
				}

				sort.SliceStable(sortedCats, func(i, j int) bool {

					return sortedCats[i].CategoryOrder < sortedCats[j].CategoryOrder
				})

				for _, category := range sortedCats {
					if len(category.Channels) < 1 {
						continue
					}

					if category.CategoryName == "" {
						category.CategoryName = "No category"
					}

					// itcat := addSubRow(it, "", category.CategoryName, category.CategoryID)
					itcat := addSubRow(it, nil, category.CategoryName, 0)

					sort.SliceStable(category.Channels, func(i, j int) bool {
						return category.Channels[i].Position < category.Channels[j].Position
					})

					for _, channel := range category.Channels {
						var shebang = " #"
						if channel.NSFW {
							shebang = "!#"
						}

						_ = addSubRow(itcat, nil, shebang+channel.Name, channel.ID)
					}
				}
			}

			w.ShowAll()
		}, window)

		_Init = true
	}

	loadGuildImages()
}

func loadGuildImages() {
	if DataSaving {
		return
	}

	// err = treeStore.SetValue(i, 0, icon)
	// if err != nil {
	// 	log.Fatal("Unable set value:", err)
	// }

	var (
		wg = sync.WaitGroup{}
	)

	glib.IdleAdd(func() {
		itLy1, ok := treeStore.GetIterFirst()
		if ok {
			for treeStore.IterNext(itLy1) {
				val, err := treeStore.GetValue(itLy1, 2)
				if err != nil {
					warn(err, "Failed to get treeStore value")
					continue
				}

				ref, err := val.GoValue()
				if err != nil {
					warn(err, "Can't convert C value to Go")
					continue
				}

				guid, ok := ref.(int64)
				if !ok {
					warn("Can't parse Guild ID")
					continue
				}

				guid = -guid

				if gu, err := d.State.Guild(guid); err == nil {
					if gu.Icon == "" {
						continue
					}

					it, _ := itLy1.Copy()

					wg.Add(1)
					go func(it *gtk.TreeIter) {
						sema <- struct{}{}
						defer func() {
							wg.Done()
							<-sema
						}()

						guildIconURL := fmt.Sprintf("https://cdn.discordapp.com/icons/%d/%s.png", guid, gu.Icon)

						pb, err := getPixbufFromURL(
							guildIconURL,
							-1, -1,
							25,
						)

						if err != nil {
							warn(err)
							return
						}

						glib.IdleAdd(func() {
							if err := treeStore.SetValue(it, 0, pb); err != nil {
								warn(err)
								return
							}
						})
					}(it)
				} else {
					log.Println(err)
					continue
				}
			}
		} else {
			log.Println("Not ok")
		}
	})

	wg.Wait()

	// for _, dmCh := range d.State.PrivateChannels {
	// 	var userstr []string
	// 	for _, u := range dmCh.Recipients {
	// 		userstr = append(userstr, u.Username)
	// 	}

	// 	_ = addSubRow(it, nil, strings.Join(userstr, ", "), dmCh.ID)
	// }

	return
}
