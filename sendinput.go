package main

import (
	"strings"

	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
)

func sendInput(input *gtk.Entry) {

	// buffer, err := input.GetBuffer()
	// if err != nil {
	// 	warn("Failed getting input buffer", err.Error())
	// 	return
	// }

	// s, e := buffer.GetBounds()

	text, err := input.GetText()
	if err != nil {
		warn("Failed getting text from input buffer", err.Error())
		return
	}

	if text == "" {
		return
	}

	// messagebox := addMsg(&discordgo.Message{
	// 	Content: text,
	// }, true)

	// go func(msgbox *gtk.Box) {
	go func() {
		_, err := d.ChannelMessageSend(
			currentChannel,
			strings.TrimSpace(text),
		)
		if err == nil {
			// glib.IdleAdd(func(msgbox *gtk.Box) {
			// 	chbox.Remove(msgbox)
			// }, msgbox)

			return
		}

		// glib.IdleAdd(func(msgbox *gtk.Box) {
		// 	msgbox.SetProperty("opacity", 1)
		// 	msgbox.GetChildren().Foreach(func(item interface{}) {
		// 		label, ok := item.(*gtk.Label)
		// 		if ok {
		// 			label.SetMarkup(fmt.Sprintf(
		// 				"<span color=\"red\"><b>%s</b> %s</span>",
		// 				d.State.User.Username,
		// 				html.EscapeString(text),
		// 			))
		// 		}
		// 	})

		warn("Failed sending message: "+err.Error(), text)
		// }, msgbox)
	}()
	// }(messagebox)
	glib.IdleAdd(func() {
		input.SetText("")
	})
}
