package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"unsafe"

	"github.com/gotk3/gotk3/gdk"
	"github.com/pkg/browser"

	"github.com/RumbleFrog/discordgo"
	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
)

const (
	// ImgLimit The maximum dimensions (width OR height) for images
	ImgLimit int = 400
)

func warn(intr interface{}, content ...string) {
	firstLine := ""

	if err, ok := intr.(error); ok {
		log.Println(err)
		fmt.Println(strings.Join(content, "\n"))

		firstLine = err.Error()
		if firstLine == "cgo returned unexpected nil pointer" {
			return
		}
	} else {
		log.Println(strings.Join(content, "\n")) // todo: make this a gtk dialog
		firstLine = fmt.Sprintf("%v", intr)
	}

	glib.IdleAdd(func(w *gtk.Window) {
		dialog := gtk.MessageDialogNew(
			w,                   // parent
			gtk.DIALOG_MODAL,    // a model dialog, independent of window
			gtk.MESSAGE_WARNING, // type Warning (function is called warn, duh)
			gtk.BUTTONS_OK,      // OK button only
			firstLine,           // content
		)

		if len(content) > 1 {
			dialog.FormatSecondaryText(strings.Join(content[1:], "\n"))
		}

		if resp := dialog.Run(); resp == gtk.RESPONSE_OK {
			dialog.Destroy()
		}

		w.ShowAll()
	}, window)
}

func upload(uri string) {
	file, err := os.OpenFile(
		strings.TrimPrefix(uri, "file://"),
		os.O_RDONLY,
		os.ModePerm,
	)

	if err != nil {
		warn(err.Error())
		return
	}

	defer file.Close()

	if _, e := d.ChannelMessageSendComplex(
		currentChannel,
		&discordgo.MessageSend{
			File: &discordgo.File{
				Name:   file.Name(),
				Reader: file,
			},
		},
	); e != nil {
		die(err)
	}
}

func scrollchat() {
	adj := sc.GetVAdjustment()
	adj.SetValue(adj.GetUpper() + 120)

}

func getFilename(url string) string {
	s := strings.Split(url, "/")
	return s[len(s)-1]
}

func getPixbufFromURL(url string, w, h, limit int) (*gdk.Pixbuf, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	pbl, err := gdk.PixbufLoaderNew()
	if err != nil {
		return nil, err
	}

	switch {
	case w < 0, h < 0:
		pbl.SetSize(limit, limit)
	case w == 0, h == 0:
		break
	case w > limit:
		pbl.SetSize(limit, int(float64(limit)/(float64(w)/float64(h))))
	case h > limit:
		pbl.SetSize(int(float64(limit)/(float64(w)/float64(h))), limit)
	}

	if _, err := pbl.Write(bytes); err != nil {
		return nil, err
	}

	return pbl.GetPixbuf()
}

func getImageFromURL(url string, w, h, limit int) (*gtk.Image, error) {
	pb, err := getPixbufFromURL(url, w, h, limit)
	if err != nil {
		return nil, err
	}

	img, err := gtk.ImageNewFromPixbuf(pb)
	if err != nil {
		return nil, err
	}

	img.SetHAlign(gtk.ALIGN_START)
	img.SetMarginBottom(5)
	img.SetMarginStart(5)
	img.SetMarginEnd(5)

	// menu, err := viewOriginalMenu(url)
	// if err == nil {
	// 	img.Connect("activate", func() {
	// 		glib.IdleAdd(func() {
	// 			menu.ShowAll()

	// 			menu.PopupAtWidget(
	// 				img,
	// 				0, 0,
	// 				nil,
	// 			)
	// 		})
	// 	})
	// }

	return img, nil
}

func viewOriginalMenu(orig string) (*gtk.Menu, error) {
	menu, err := gtk.MenuNew()
	if err != nil {
		return nil, err
	}

	origitem, err := gtk.MenuItemNewWithLabel("View Original")
	if err != nil {
		return nil, err
	}

	menu.Append(origitem)

	origitem.Connect("activate", func() {
		log.Println(browser.OpenURL(orig))
	})

	return menu, nil
}

func mkFrame(name string) (*gtk.Frame, error) {
	frame, err := gtk.FrameNew(name)
	if err != nil {
		return nil, err
	}

	frame.SetHAlign(gtk.ALIGN_START)
	frame.SetMarginBottom(3)
	frame.SetMarginTop(3)
	frame.SetMarginStart(2)

	return frame, nil
}

func getLabelType() glib.Type {
	label, _ := gtk.LabelNew("")
	return label.TypeFromInstance()
}

func setStatusButton() {
	if status == nil {
		return
	}

	var color = "#43b581" // green
	if Busy {
		color = "#f04747" // red
	}

	glib.IdleAdd(func() {
		status.GetChildren().Foreach(func(item interface{}) {
			if item.(*gtk.Widget).IsA(getLabelType()) {
				label := (*gtk.Label)(unsafe.Pointer(item.(*gtk.Widget)))
				label.SetMarkup("<span color=\"" + color + "\">⬤</span>")
			}
		})
	})
}

type colContent struct {
	Header     string
	ID         int
	Type       glib.Type
	Hidden     bool
	ShowHeader bool
	Monospace  bool
}

// Add a column to the tree view (during the initialization of the tree view)
func createColumn(title string, id int, v bool, m bool) *gtk.TreeViewColumn {
	cellRenderer, err := gtk.CellRendererTextNew()
	die(err)

	if m {
		cellRenderer.SetProperty("font", "Monospace")
		cellRenderer.SetProperty("family", "Monospace")
	}

	column, err := gtk.TreeViewColumnNewWithAttribute(title, cellRenderer, "text", id)
	die(err)

	column.SetVisible(v)
	column.SetResizable(true)

	return column
}

// Add a column to the tree view (during the initialization of the tree view)
func createPbColumn(title string, id int) *gtk.TreeViewColumn {
	cellRenderer, err := gtk.CellRendererPixbufNew()
	die(err)

	column, err := gtk.TreeViewColumnNewWithAttribute(title, cellRenderer, "pixbuf", id)
	die(err)

	column.SetResizable(false)
	column.SetFixedWidth(55)

	return column
}

// Creates a tree view and the list store that holds its data
func setupTreeView(cc []colContent) (*gtk.TreeView, *gtk.TreeStore) {
	treeView, err := gtk.TreeViewNew()
	if err != nil {
		log.Fatal("Unable to create tree view:", err)
	}

	var types []glib.Type

	for _, col := range cc {
		switch col.Type {
		case glib.TYPE_OBJECT:
			treeView.AppendColumn(createPbColumn(col.Header, col.ID))
		default:
			treeView.AppendColumn(createColumn(col.Header, col.ID, !col.Hidden, col.Monospace))
		}

		types = append(types, col.Type)
	}

	treeView.SetHeadersVisible(cc[0].ShowHeader)
	treeView.SetProperty("activate-on-single-click", true)
	treeView.Connect("row-activated", func(tv *gtk.TreeView, tp *gtk.TreePath, tvc *gtk.TreeViewColumn) {
		if treeView.RowExpanded(tp) {
			treeView.CollapseRow(tp)
			return
		}

		treeView.ExpandRow(tp, false)
	})

	// Creating a tree store. This is what holds the data that will be shown on our tree view.
	treeStore, err := gtk.TreeStoreNew(types...)
	if err != nil {
		log.Fatal("Unable to create tree store:", err)
	}

	treeView.SetModel(treeStore)

	return treeView, treeStore
}

// // Append a row to the list store for the tree view
// func addRow(icon, name string) {
// 	// Get an iterator for a new row at the end of the list store
// 	iter := listStore.Append()

// 	// Set the contents of the list store row that the iterator represents
// 	err := listStore.Set(
// 		iter,
// 		[]int{0, 1},
// 		[]interface{}{icon, name},
// 	)

// 	die(err)
// }

// Append a toplevel row to the tree store for the tree view
func addRow(icon *gdk.Pixbuf, name string, id int64) *gtk.TreeIter {
	return addSubRow(nil, icon, name, id)
}

// Append a sub row to the tree store for the tree view
func addSubRow(iter *gtk.TreeIter, icon *gdk.Pixbuf, name string, id int64) *gtk.TreeIter {
	var i *gtk.TreeIter

	// Get an iterator for a new row at the end of the list store
	i = treeStore.Append(iter)

	var err error

	// err = treeStore.SetValue(i, 0, icon)
	// if err != nil {
	// 	log.Fatal("Unable set value:", err)
	// }

	err = treeStore.SetValue(i, 1, name)
	if err != nil {
		log.Fatal("Unable set value:", err)
	}

	err = treeStore.SetValue(i, 2, id)
	if err != nil {
		log.Fatal("Unable set value:", err)
	}

	return i
}

// Handle selection
func treeSelectionChangedCB(selection *gtk.TreeSelection, tv *gtk.TreeView) {
	var iter *gtk.TreeIter
	var model gtk.ITreeModel
	var ok bool
	model, iter, ok = selection.GetSelected()
	if ok {
		value, err := model.(*gtk.TreeModel).GetValue(iter, 2)
		die(err)

		intf, err := value.GoValue()
		die(err)

		channelID, ok := intf.(int64)
		if !ok {
			die(errors.New("Invalid type"))
		}

		log.Printf("treeSelectionChangedCB: selected path: %d\n", channelID)

		if channelID < 1 {
			return
		}

		loadMsgs(channelID)
	} else {
		log.Println("Failed to get selected channel")
	}
}

func marginLabel(l *gtk.Label, marginTopDown, marginLeftRight int) {
	l.SetMarginTop(marginTopDown)
	l.SetMarginBottom(0)

	l.SetMarginStart(marginLeftRight)
	l.SetMarginEnd(marginLeftRight)
}

func marginButton(l *gtk.Button, marginLeftRight int) {
	// l.SetMarginStart(marginLeftRight)
	// l.SetMarginEnd(marginLeftRight)
}
