package main

import (
	"log"

	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
)

// var (
// 	test, _ = gtk.LabelNew("asdasd")
// )

func initWindow(w *gtk.Window) (*gtk.Grid, error) {
	grid, err := gtk.GridNew()
	if err != nil {
		return nil, err
	}

	grid.SetOrientation(gtk.ORIENTATION_HORIZONTAL)
	// box.SetHAlign(gtk.ALIGN_FILL)
	// box.SetVAlign(gtk.ALIGN_FILL)

	w.Add(grid)

	scrollTree, err := gtk.ScrolledWindowNew(nil, nil)
	if err != nil {
		return nil, err
	}

	scrollTree.SetPolicy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
	scrollTree.SetProperty("width-request", 220)

	grid.Attach(scrollTree, 0, 0, 1, 1)

	separator, err := gtk.SeparatorNew(gtk.ORIENTATION_VERTICAL)
	if err != nil {
		return nil, err
	}

	grid.AttachNextTo(separator, scrollTree, gtk.POS_RIGHT, 1, 1)

	treeView, ts := setupTreeView([]colContent{
		colContent{
			Header:     "Icon",
			ID:         0,
			Type:       glib.TYPE_OBJECT,
			ShowHeader: true,
		},
		colContent{
			Header:     "Name",
			ID:         1,
			Type:       glib.TYPE_STRING,
			ShowHeader: true,
		},
		colContent{
			Header:     "ID",
			ID:         2,
			Type:       glib.TYPE_INT64,
			Hidden:     true,
			ShowHeader: true,
		},
	})

	treeView.SetProperty("enable-tree-lines", true)
	// treeView.SetProperty("enable-grid-lines", "GTK_TREE_VIEW_GRID_LINES_VERTICAL")

	scrollTree.Add(treeView)

	treeStore = ts

	selection, err := treeView.GetSelection()
	if err != nil {
		return nil, err
	}

	selection.SetMode(gtk.SELECTION_SINGLE)
	selection.Connect("changed", treeSelectionChangedCB, treeView)

	rightBox, err := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 0)
	if err != nil {
		return nil, err
	}

	rightBox.SetVAlign(gtk.ALIGN_END)

	scrollChat, err := gtk.ScrolledWindowNew(nil, nil)
	if err != nil {
		return nil, err
	}

	scrollChat.SetPolicy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
	scrollChat.SetProperty("propagate-natural-width", true)
	scrollChat.SetProperty("propagate-natural-height", true)
	scrollChat.SetHExpand(true)
	scrollChat.SetVExpand(true)

	rightBox.PackStart(scrollChat, true, true, 0)

	sc = scrollChat

	boxch, err := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 0)
	if err != nil {
		return nil, err
	}

	boxch.SetProperty("margin", 12)

	chbox = boxch
	scrollChat.Add(chbox)

	/*
	 * INPUT BOX for the entire bottom row
	 */
	inputbox, err := gtk.BoxNew(gtk.ORIENTATION_HORIZONTAL, 0)
	if err != nil {
		return nil, err
	}

	inputbox.SetProperty("margin", 12)

	rightBox.PackEnd(inputbox, true, false, 0)

	/*
	 * file upload button +
	 */
	filebtn, err := gtk.ButtonNewWithLabel("+")
	if err != nil {
		return nil, err
	}

	filebtn.Connect("clicked", func() {
		glib.IdleAdd(func() {
			dialog, err := gtk.FileChooserNativeDialogNew(
				"Upload a file",
				window,
				gtk.FILE_CHOOSER_ACTION_OPEN,
				"Upload",
				"Cancel",
			)

			if err != nil {
				log.Fatalln("Failed creating File uploader:", err.Error())
				return
			}

			if dialog.Run() == int(gtk.RESPONSE_ACCEPT) {
				// Async
				go upload(dialog.FileChooser.GetURI())
			}
		})
	})

	marginButton(filebtn, 8)
	inputbox.PackStart(filebtn, false, false, 0)

	/*
	 * actual input box
	 */
	input, err := gtk.EntryNew()
	if err != nil {
		return nil, err
	}

	input.SetProperty("border-width", 6)
	input.Connect("activate", func() {
		sendInput(input)
	})
	input.Connect("insert-at-cursor", func() {
		text, err := input.GetText()
		if err != nil {
			log.Println(err)
			return
		}

		log.Println(text)
	})

	inputbox.PackStart(input, true, true, 0)

	// fucks sake gotk3
	// autocomplete, err := gtk.Comple

	/*
	 * emoji button ":D"
	 */
	emojibtn, err := gtk.ButtonNewWithLabel(":D")
	if err != nil {
		return nil, err
	}

	emojibtn.Connect("clicked", func() {
		if _, err := input.Emit("insert-emoji"); err != nil {
			log.Println(err)
		}
	})

	/*
	 * send button >
	 */
	sendbtn, err := gtk.ButtonNewWithLabel("Send")
	if err != nil {
		return nil, err
	}

	sendbtn.Connect("clicked", func() {
		sendInput(input)
	})

	marginButton(sendbtn, 8)
	inputbox.PackEnd(sendbtn, false, false, 0)

	marginButton(emojibtn, 8)
	inputbox.PackEnd(emojibtn, false, false, 0)

	grid.AttachNextTo(rightBox, separator, gtk.POS_RIGHT, 1, 1)

	return grid, nil
}
