package main

import (
	"flag"
	"log"
	"os"

	"github.com/RumbleFrog/discordgo"
	"github.com/davecgh/go-spew/spew"
	"github.com/gen2brain/beeep"
	"github.com/gotk3/gotk3/gtk"
	keyring "github.com/zalando/go-keyring"
)

const (
	// AppName is used to store gtkcord keyrings
	AppName string = "gtkcord"

	// CSD determines whether or not the app should use a client-side decoration
	CSD bool = true
)

var (
	// Busy when true, won't send a notification
	Busy = false

	d *discordgo.Session

	window    *gtk.Window
	spinner   *gtk.Spinner
	treeStore *gtk.TreeStore
	sc        *gtk.ScrolledWindow
	chbox     *gtk.Box
	status    *gtk.Button

	tmp = func() string {
		return "/tmp/gtkcord/" + os.Getenv("USER")
	}()
)

func die(e error) {
	if e != nil {
		log.Fatalln(e)
	}
}

func main() {
	log.SetFlags(log.Lshortfile)

	if len(os.Args) > 1 {
		switch os.Args[1] {
		case "rmkeyring":
			switch err := keyring.Delete(AppName, "token"); err {
			case nil:
				log.Println("Keyring deleted.")
				return
			default:
				log.Panicln(err)
			}
		}
	}

	token := flag.String("t", "", "Discord token (1)")

	username := flag.String("u", "", "Username/Email (2)")
	password := flag.String("p", "", "Password (2)")

	flag.Parse()

	// settings, _ := gtk.SettingsGetDefault()
	// settings.SetPropertyValue("gtk-application-prefer-dark-theme", true)

	var err error

	var login []interface{}

	k, err := keyring.Get(AppName, "token")
	if err != nil {
		if err != keyring.ErrNotFound {
			log.Println(err.Error())
		}

		switch {
		case *token != "":
			login = append(login, *token)
		case *username != "", *password != "":
			login = append(login, *username)
			login = append(login, *password)

			if *token != "" {
				login = append(login, *token)
			}
		default:
			log.Fatalln("Token OR username + password missing! Refer to -h")
		}
	} else {
		login = append(login, k)
	}

	d, err = discordgo.New(login...)
	if err != nil {
		log.Panicln(err)
	}

	d.AddHandler(onReady)
	d.AddHandler(EditMsg)
	d.AddHandler(func(s *discordgo.Session, m *discordgo.MessageCreate) {
		if rstore.Check(m.Author, RelationshipBlocked) {
			return
		}

		if !Busy {
			for _, mention := range m.Mentions {
				if mention.ID == d.State.User.ID {
					c, e := m.ContentWithMoreMentionsReplaced(s)
					if e != nil {
						c = m.ContentWithMentionsReplaced()
					}

					if err := beeep.Notify(m.Author.Username+" mentioned you", c, ""); err != nil {
						log.Println(err)
					}
				}
			}
		}

		if m.ChannelID != currentChannel {
			return
		}

		addMsg(m.Message, false, nil)
	})

	d.AddHandler(func(s *discordgo.Session, m *discordgo.PresenceUpdate) {
		if m.User.ID != d.State.User.ID {
			return
		}

		switch m.Status {
		case discordgo.StatusOnline:
			Busy = false
		default:
			Busy = true
		}

		setStatusButton()
	})

	// initialize tmp
	go func() {
		if err := os.MkdirAll(tmp, os.ModePerm); err != nil {
			panic(err)
		}
	}()

	// initialize gtk
	gtk.Init(nil)

	window, err = gtk.WindowNew(gtk.WINDOW_TOPLEVEL)
	die(err)

	window.SetDefaultSize(912, 600)

	window.Connect("destroy", func() {
		gtk.MainQuit()
	})

	headerbar, err := gtk.HeaderBarNew()
	die(err)

	headerbar.SetShowCloseButton(CSD)
	headerbar.SetTitle("gtkcord-test")

	status, err = gtk.ButtonNewWithLabel("⬤")
	die(err)

	if w, err := status.GetChild(); err == nil {
		w.SetProperty("use-markup", true)

		// if label, ok := item.(*gtk.Label); ok {
		// 	log.Println("Setting")
		// 	label.SetUseMarkup(true)
		// }
	} else {
		log.Println(err)
	}

	status.Connect("clicked", func() {
		var st = discordgo.StatusDoNotDisturb
		if Busy {
			st = discordgo.StatusOnline
		}

		if settings, err := d.UserUpdateStatus(st); err != nil {
			log.Println(err.Error())
		} else {
			spew.Dump(st, settings)
		}

		Busy = !Busy

		setStatusButton()
	})

	headerbar.PackStart(status)

	window.SetTitlebar(headerbar)

	spinner, err = gtk.SpinnerNew()
	if err != nil {
		warn("Failed to spawn a spinner", err.Error())
	}

	window.Add(spinner)

	spinner.Start()

	window.ShowAll()

	if err := d.Open(); err != nil {
		warn("Failed to connect to Discord", err.Error())
	}

	defer d.Close()

	log.Println("Storing token inside keyring...")
	if err := keyring.Set(AppName, "token", d.Token); err != nil {
		log.Println("Failed to set keyring! Continuing anyway...", err.Error())
	}

	defer gtk.MainQuit()
	gtk.Main()
}
