package main

import (
	"fmt"
	"html"
	"log"
	"strings"
	"sync"

	"github.com/RumbleFrog/discordgo"
	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
	"github.com/gotk3/gotk3/pango"
)

func buildMsg(m *discordgo.Message, messagebox *gtk.Box, dim, edited bool, mutex *sync.Mutex) {
	log.Println("Doing:", m.ID)
	c := Markdown(fmtMsg(m))

	if edited {
		c += " <span color=\"gray\" size=\"small\">(edited)</span>"
		log.Println("Edited:", c)
	}

	glib.IdleAdd(func() {
		message, err := gtk.LabelNew(c)
		if err != nil {
			log.Panicln(err)
		}

		messagebox.PackStart(message, true, true, 0)

		message.SetUseMarkup(true)
		message.SetHAlign(gtk.ALIGN_START)
		message.SetVAlign(gtk.ALIGN_START)
		message.SetXAlign(0.0)
		message.SetLineWrap(true)
		message.SetLineWrapMode(pango.WRAP_WORD_CHAR)
		message.SetProperty("selectable", true)
		marginLabel(message, 4, 2)
	})

	// if err := os.MkdirAll(fmt.Sprintf("%s/%d", tmp, m.ID), os.ModePerm); err != nil {
	// 	panic(err)
	// }

	// sema <- struct{}{}
	// defer func() {
	// 	wg.Done()
	// 	<-sema
	// }()

	if mutex != nil {
		mutex.Unlock()
	}

	for _, a := range m.Attachments {
		attachmentFrame, err := mkFrame(a.Filename)
		if err != nil {
			warn(err)
			return
		}

		switch a.Width {
		case 0: // not an image
			label, err := gtk.LabelNew("")
			if err == nil {
				label.SetMarkup(fmt.Sprintf(
					"<a href=\"%s\">%s</a>",
					html.EscapeString(a.URL),
					html.EscapeString(a.Filename),
				))

				attachmentFrame.Add(label)
			}
		default:
			img, err := getImageFromURL(
				a.ProxyURL,
				a.Width, a.Height,
				ImgLimit,
			)

			if err == nil {
				attachmentFrame.Add(img)
			} else {
				warn(err)
			}
		}

		if err == nil {
			if _, err := glib.IdleAdd(func(a *gtk.Frame) {
				messagebox.PackEnd(a, false, false, 0)
				chbox.ShowAll()
				scrollchat()
			}, attachmentFrame); err != nil {
				warn(err)
				return
			}
		} else {
			warn(err)
		}
	}

	return

	// defer wg.Done()

	for _, e := range m.Embeds {
		frame, err := mkFrame("")
		if err != nil {
			warn(err)
			return
		}

		embedbox, err := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 0)
		if err != nil {
			warn(err)
			return
		}

		embedbox.SetHAlign(gtk.ALIGN_START)
		embedbox.SetMarginBottom(5)
		embedbox.SetMarginStart(5)
		embedbox.SetMarginEnd(5)

		frame.Add(embedbox)

		var lines []string

		if e.Author != nil {
			// var img string
			// if e.Author.IconURL != "" {
			// 	img = "<a href=\"" + e.Author.IconURL + "\"><img src=\"" + e.Author.ProxyIconURL + "\"/></a>"
			// } :(

			lines = append(
				lines,
				fmt.Sprintf(
					"<a href=\"%s\"><b><span foreground=\"#BBB\">%s</span></b></a>",
					html.EscapeString(e.Author.URL),
					html.EscapeString(e.Author.Name),
				),
			)
		}

		if e.Title != "" {
			lines = append(
				lines,
				fmt.Sprintf(
					"<b><a href=\"%s\"><span foreground=\"#3745EB\">%s</span></a></b>",
					html.EscapeString(e.URL),
					html.EscapeString(e.Title),
				),
			)
		}

		if e.Description != "" {
			lines = append(
				lines,
				fmt.Sprintf(
					"%s",
					// parseEmoji(strings.Replace(e.Description, ">", "\\>", -1)),
					fullFmt(e.Description, m),
				),
			)
		}

		if len(e.Fields) > 0 {
			for _, f := range e.Fields {
				lines = append(
					lines,
					fmt.Sprintf(
						"\t%s\n\t<span foreground=\"#CCC\">%s</span>",
						html.EscapeString(f.Name),
						fullFmt(f.Value, m),
					),
				)
			}
		}

		if e.Footer != nil {
			lines = append(
				lines,
				fmt.Sprintf(
					"<a href=\"%s\"><span size=\"small\" foreground=\"gray\">%s</span></a>",
					html.EscapeString(e.Footer.IconURL),
					html.EscapeString(e.Footer.Text),
				),
			)
		}

		label, err := gtk.LabelNew("")
		if err != nil {
			warn(err)
			return
		}

		label.SetMarkup(strings.Join(lines, "\n"))
		label.SetLineWrap(true)
		label.SetLineWrapMode(pango.WRAP_WORD_CHAR)
		label.SetHAlign(gtk.ALIGN_START)

		embedbox.PackStart(label, false, false, 0)

		if e.Thumbnail != nil {
			img, err := getImageFromURL(
				e.Thumbnail.ProxyURL,
				e.Thumbnail.Width,
				e.Thumbnail.Height,
				ImgLimit,
			)

			if err == nil {
				embedbox.PackEnd(img, false, false, 0)
			} else {
				warn(err)
			}
		}

		if e.Image != nil {
			img, err := getImageFromURL(
				e.Image.ProxyURL,
				e.Image.Width,
				e.Image.Height,
				ImgLimit,
			)

			if err == nil {
				embedbox.PackEnd(img, false, false, 0)
			} else {
				warn(err)
			}
		}

		if _, err := glib.IdleAdd(func(f *gtk.Frame) {
			messagebox.PackEnd(f, false, false, 0)
			chbox.ShowAll()
			scrollchat()
		}, frame); err != nil {
			warn(err)
			return
		}
	}
	// if err := os.RemoveAll(fmt.Sprintf("%s/%d", tmp, m.ID)); err != nil {
	// 	warn(err.Error())
	// }

	// it := chatbuf.GetEndIter()
	// chatbuf.Insert(it, fmt.Sprintf("<b>%s</b> %s\n", author, html.EscapeString(content)))

	// // Get an iterator for a new row at the end of the list store
	// iter := listStoreChat.Prepend()
	// // Set the contents of the list store row that the iterator represents
	// err := listStoreChat.Set(
	// 	iter,
	// 	[]int{0, 1, 2},
	// 	[]interface{}{author, content, id},
	// )

	// die(err)

	return
}
