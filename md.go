package main

import (
	pangoup "gitlab.com/diamondburned/pangoup"
)

// Markdown parses markdown into hopefully Pango-compatible formats
func Markdown(input string) string {
	return string(pangoup.ToHTML(
		[]byte(input),
		nil, nil,
	))
}
