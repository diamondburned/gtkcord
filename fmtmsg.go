package main

import (
	"regexp"
	"strconv"
	"strings"

	"github.com/RumbleFrog/discordgo"
)

var (
	escaper = strings.NewReplacer(
		//"<", "&lt;",
		// ">", "&gt;",
		"&", "&amp;",
	)
)

func replaceUsername(str string, gid int64, mentions []*discordgo.User) string {
	for _, user := range mentions {
		nick := user.Username

		member, err := d.State.Member(gid, user.ID)
		if err == nil && member.Nick != "" {
			nick = member.Nick
		}

		str = strings.NewReplacer(
			"<@"+strconv.FormatInt(user.ID, 10)+">", "@"+(user.Username)+"",
			"<@!"+strconv.FormatInt(user.ID, 10)+">", "@"+(nick)+"",
		).Replace(str)
	}

	return str
}

func replaceRoles(c string, gid int64, sl discordgo.IDSlice) string {
	for _, roleID := range sl {
		role, err := d.State.Role(gid, roleID)
		if err != nil || !role.Mentionable {
			continue
		}

		c = strings.Replace(c, "<@&"+strconv.FormatInt(role.ID, 10)+">", "@"+(role.Name)+"", -1)
	}

	return c
}

func replaceChannels(c string) string {
	c = regexp.MustCompile("<#[^>]*>").ReplaceAllStringFunc(c, func(mention string) string {
		id, err := strconv.ParseInt(mention[2:len(mention)-1], 10, 64)
		if err != nil {
			return escaper.Replace(mention)
		}

		channel, err := d.State.Channel(id)
		if err != nil || channel.Type == discordgo.ChannelTypeGuildVoice {
			return escaper.Replace(mention)
		}

		return "#" + escaper.Replace(channel.Name) + ""
	})

	return c
}

func fullFmt(c string, m *discordgo.Message) string {
	if d.StateEnabled {
		channel, err := d.State.Channel(m.ChannelID)
		if err == nil {
			c = replaceRoles(
				replaceUsername(
					replaceChannels(c),
					channel.GuildID,
					m.Mentions,
				),
				channel.GuildID,
				m.MentionRoles,
			)
		}
	}

	return escaper.Replace(c)
}

func fmtMsg(m *discordgo.Message) string {
	return fullFmt(
		m.ContentWithMentionsReplaced(),
		m,
	)
}
