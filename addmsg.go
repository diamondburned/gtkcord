package main

import (
	"fmt"
	"html"
	"log"
	"sync"

	"github.com/RumbleFrog/discordgo"
	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/pango"

	"github.com/gotk3/gotk3/gtk"
)

var (
	messages = []*MessageElements{}
)

// MessageElements contains pointers to appropriate messages
type MessageElements struct {
	MessagesContainer *gtk.Box

	MessagesBox *gtk.Box
	Messages    []MessageElement

	AuthorID int64
	Author   *gtk.Label
}

type MessageElement struct {
	Position   int
	MessageID  int64
	MessageBox *gtk.Box
}

func addMsg(m *discordgo.Message, dim bool, mutex *sync.Mutex) {
	if mutex != nil {
		mutex.Lock()
	}

	var err error
	var msgelem = &MessageElements{}

	if len(messages) > 0 && messages[len(messages)-1].AuthorID == m.Author.ID /*&& msgelem.MessagesContainer != nil && msgelem.MessagesBox != nil*/ {
		msgelem = messages[len(messages)-1]
	} else {
		// Add to global array
		messages = append(messages, msgelem)

		msgelem.AuthorID = m.Author.ID

		msgelem.MessagesContainer, err = gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 0)
		die(err)

		msgelem.MessagesContainer.SetMarginTop(8)
		msgelem.MessagesContainer.SetMarginBottom(0)
		msgelem.MessagesContainer.SetMarginStart(10)
		msgelem.MessagesContainer.SetMarginEnd(10)
		msgelem.MessagesContainer.SetHAlign(gtk.ALIGN_START)
		msgelem.MessagesContainer.SetVAlign(gtk.ALIGN_START)

		topbox, err := gtk.BoxNew(gtk.ORIENTATION_HORIZONTAL, 0)
		die(err)

		topbox.SetHAlign(gtk.ALIGN_START)
		topbox.SetVAlign(gtk.ALIGN_START)
		topbox.SetMarginTop(5)
		topbox.SetMarginStart(2)
		topbox.SetMarginEnd(2)

		msgelem.MessagesContainer.PackStart(topbox, true, true, 0)

		msgelem.Author, err = gtk.LabelNew(fmt.Sprintf(
			"<span underline=\"none\" color=\"#%X\"><b><a href=\"#\">%s</a></b></span>",
			16711422,
			m.Author.Username,
		))

		die(err)

		msgelem.Author.SetUseMarkup(true)
		msgelem.Author.SetHAlign(gtk.ALIGN_START)
		msgelem.Author.SetVAlign(gtk.ALIGN_START)
		msgelem.Author.SetXAlign(0.0)
		msgelem.Author.SetLineWrap(true)
		msgelem.Author.SetLineWrapMode(pango.WRAP_WORD_CHAR)
		msgelem.Author.SetProperty("selectable", true)
		msgelem.Author.SetProperty("max-width-chars", 60)

		msgelem.Author.Connect("activate-link", func() {
			log.Println("Username clicked, user ID is", m.Author.ID)
		})

		// Add the author name into the Top box
		topbox.PackStart(msgelem.Author, true, false, 0)

		go func(e *MessageElements) {
			author, color := getUserData(m) // todo: put this in a goroutine

			glib.IdleAdd(func(el *MessageElements) {
				el.Author.SetLabel(fmt.Sprintf(
					"<span underline=\"none\" color=\"#%X\"><b><a href=\"#\">%s</a></b></span>",
					color,
					html.EscapeString(author),
				))

				el.Author.ShowAll()
			}, e)
		}(msgelem)

		var timeString = "Can't parse time!"

		timestamp, err := m.Timestamp.Parse()
		if err == nil {
			timeString = timestamp.Format("15:04PM 02/01/2006")
		}

		timelabel, err := gtk.LabelNew("<span color=\"gray\" size=\"small\">" + timeString + "</span>")
		die(err)

		timelabel.SetUseMarkup(true)
		timelabel.SetHAlign(gtk.ALIGN_END)
		timelabel.SetVAlign(gtk.ALIGN_END)
		timelabel.SetXAlign(0.0)
		timelabel.SetMarginStart(8)
		timelabel.SetProperty("selectable", true)

		// Add the timestamp label into the Top box
		topbox.PackStart(timelabel, true, true, 0)

		msgelem.MessagesBox, err = gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 0)
		die(err)

		msgelem.MessagesBox.SetHAlign(gtk.ALIGN_START)
		msgelem.MessagesBox.SetVAlign(gtk.ALIGN_START)

		msgelem.MessagesContainer.PackEnd(msgelem.MessagesBox, true, false, 0)

		glib.IdleAdd(func(b *gtk.Box) {
			chbox.PackStart(b, true, false, 0)
			chbox.ShowAll()
		}, msgelem.MessagesContainer)
	}

	messagebox, err := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 0)
	die(err)

	messagebox.SetHAlign(gtk.ALIGN_START)
	messagebox.SetVAlign(gtk.ALIGN_START)
	messagebox.SetName(fmt.Sprintf("%d", m.ID))

	if dim {
		messagebox.SetProperty("opacity", 0.5)
	}

	wrap, err := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 0)

	wrap.SetHAlign(gtk.ALIGN_START)
	wrap.SetVAlign(gtk.ALIGN_START)

	buildMsg(m, wrap, dim, false, mutex) // todo: what if it is edited?

	glib.IdleAdd(func(b *gtk.Box) {
		msgelem.MessagesBox.PackStart(messagebox, true, false, 0)
		messagebox.Add(wrap)

		// wrap.PackStart(b, true, false, 0)
		messagebox.ShowAll()
		scrollchat()
	}, messagebox)

	msgelem.Messages = append(msgelem.Messages, MessageElement{
		Position:   messagebox.GetAllocatedHeight(),
		MessageID:  m.ID,
		MessageBox: messagebox,
	})
}
